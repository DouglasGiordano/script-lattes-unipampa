# -*- coding: utf-8 -*-
import re
from pytagcloud.lang.stopwords import StopWords
from operator import itemgetter
import operator

def  get_tag_counts(text, max):
    """
    Search tags in a given text. The language detection is based on stop lists.
    This implementation is inspired by https://github.com/jdf/cue.language. Thanks Jonathan Feinberg.
    """
    """words = map(lambda x:x.lower(), re.findall(r'\r+', text, re.UNICODE))"""
    words = text.split("\n")
    s = StopWords()
    s.load_language(s.guess(words))

    counted = {}

    for word in words:
        if not s.is_stop_word(word) and len(word) > 1:
            if counted.has_key(word):
                counted[word] += 1
            else:
                counted[word] = 1
    countedNew = {}
    i = 0
    for w in sorted(counted, key=counted.get, reverse=True):
        if i < max:
            countedNew[w.decode('utf8').lower()]= counted[w]
            print w.decode('utf8').lower(), counted[w]
            i += 1
        else:
            break

    return sorted(countedNew.iteritems(), key=itemgetter(1), reverse=True)
